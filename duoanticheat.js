var tipsState = storage.local.get({tipsStorage:true});
var speechState = storage.local.get({speechStorage:true});
var cssState = storage.local.get({cssStorage:false});
const tips = '<style id="1312a">._1ihC0 {display:none;}</style>';
const speech = '<style id="1312b">._1KUxv._3ANq3 span:nth-child(2) {display: none;}</style>';
// Stylesheet generated with The Dark Reader @ https://darkreader.org/


const cssString = String(css);
var cssNode = document.createElement("STYLE");
cssNode.innerHTML = cssString;
const header = document.getElementsByTagName("head")[0];
if(tipsState){
    header.innerHTML += tips;
}
if(speechState){
    header.innerHTML += speech;
}
if(cssState){
    header.appendChild(cssNode);
}//test