# Duo Anticheat

Helps you not cheat at duolingo by removing hovering tooltips and/or slow text.


How to install: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Your_first_WebExtension#trying_it_out

Using Greasemonkey for small tests: https://www.greasespot.net/

